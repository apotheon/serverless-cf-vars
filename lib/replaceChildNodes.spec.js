/*eslint no-template-curly-in-string: "off"*/

const replaceChildNodes = require('./replaceChildNodes')
const keyStem = 'serverlesscfvarsplugin'

describe('replaceChildNodes', () => {
  describe('step0', () => {
    it('Replaces #{whatever} with {Fn::Sub: gibberish}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': 'Resource#{Name}',
            Unchanged: 'DoNotChange',
            ANumber: 2
          }
        },
      }
      const result = replaceChildNodes.step0(resources)
      expect(result.resources.AResource.Properties.Name).toEqual({'Fn::Sub': `Resource${keyStem}0`})
      expect(result.resources.AResource.Properties.Unchanged).toBe('DoNotChange')
      expect(result.resources.AResource.Properties.ANumber).toBe(2)
      expect(result.vars).toEqual({[`${keyStem}0`]: 'Name'})
    })
    it('Replaces {Fn::Sub #{whatever}} with {Fn::Sub: gibberish}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': 'Resource#{Name}'
            }
          }
        },
      }
      const result = replaceChildNodes.step0(resources)
      expect(result.resources.AResource.Properties.Name).toEqual({'Fn::Sub': `Resource${keyStem}0`})
      expect(result.vars).toEqual({[`${keyStem}0`]: 'Name'})
    })
    it('Replaces {Fn::Sub #{whatever}, whatever: value} with {Fn::Sub: gibberish, whatever: value}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': [
                'Resource#{Name}',
                { 'Name': 'theName' }
              ]
            }
          }
        },
      }
      const result = replaceChildNodes.step0(resources)
      expect(result.resources.AResource.Properties.Name).toEqual({'Fn::Sub': [`Resource${keyStem}0`, {Name: 'theName'}]})
      expect(result.vars).toEqual({[`${keyStem}0`]: 'Name'})
    })
  })

  describe('step1', () => {
    it('Replaces {Fn::Sub gibberish, whatever: value} with {Fn::Sub: ${whatever}, whatever: value}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': [
                `Resource${keyStem}0`,
                { 'Name': 'theName' }
              ]
            }
          }
        },
      }
      const vars = {[`${keyStem}0`]: 'Name'}
      const result = replaceChildNodes.step1(resources, vars)
      expect(result.AResource.Properties.Name).toEqual({'Fn::Sub': ['Resource${Name}', {Name: 'theName'}]})
    })
    it('Replaces {Fn::Sub gibberish} with {Fn::Sub: ${whatever}}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': `Resource${keyStem}0`,
            },
            Unchanged: 'DoNotChange',
            ANumber: 2
          }
        },
      }
      const vars = {[`${keyStem}0`]: 'Name'}
      const result = replaceChildNodes.step1(resources, vars)
      expect(result.AResource.Properties.Name).toEqual({'Fn::Sub': 'Resource${Name}'})
      expect(result.AResource.Properties.Unchanged).toBe('DoNotChange')
      expect(result.AResource.Properties.ANumber).toBe(2)
    })
  })

  describe('step2', () => {
    it('Replaces #{whatever} with {Fn::Sub: ${whatever}}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': 'Resource#{Name}',
            Unchanged: 'DoNotChange',
            ANumber: 2
          }
        },
      }
      const result = replaceChildNodes.step2(resources)
      expect(result.AResource.Properties.Name).toEqual({'Fn::Sub': 'Resource${Name}'})
      expect(result.AResource.Properties.Unchanged).toBe('DoNotChange')
      expect(result.AResource.Properties.ANumber).toBe(2)
    })
    it('Replaces {Fn::Sub #{whatever}} with {Fn::Sub: ${whatever}}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': 'Resource#{Name}'
            }
          }
        },
      }
      const result = replaceChildNodes.step2(resources)
      expect(result.AResource.Properties.Name).toEqual({'Fn::Sub': 'Resource${Name}'})
    })
    it('Replaces {Fn::Sub #{whatever}, whatever: value} with {Fn::Sub: ${whatever}, whatever: value}', () => {
      const resources = {
        AResource: {
          'Type': 'AWS::Resource::AResource',
          'Properties': {
            'Name': {
              'Fn::Sub': [
                'Resource#{Name}',
                { 'Name': 'theName' }
              ]
            }
          }
        },
      }
      const result = replaceChildNodes.step2(resources)
      expect(result.AResource.Properties.Name).toEqual({'Fn::Sub': ['Resource${Name}', {Name: 'theName'}]})
    })
  })
})

