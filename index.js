var replaceChildNodes = require('./lib/replaceChildNodes')

class ServerlessCfVars {
  constructor(serverless, options) {
    this.serverless = serverless
    this.options = options
    this.hooks = {
      'package:setupProviderConfiguration': this.addVarsStep0.bind(this),
      'aws:package:finalize:mergeCustomProviderResources': this.addVarsStep1.bind(this)
    }
  }

  addVarsStep0() {
    var template = this.serverless.service.provider.compiledCloudFormationTemplate
    var resResult = replaceChildNodes.step0(template.Resources)
    template.Resources = resResult.resources
    var outResult = replaceChildNodes.step0(template.Outputs, false, resResult.vars)
    template.Outputs = outResult.resources
    this.vars = outResult.vars
  }
  addVarsStep1() {
    var template = this.serverless.service.provider.compiledCloudFormationTemplate
    template.Resources = replaceChildNodes.step1(template.Resources, this.vars)
    template.Outputs = replaceChildNodes.step1(template.Outputs, this.vars)
    template.Resources = replaceChildNodes.step2(template.Resources)
    template.Outputs = replaceChildNodes.step2(template.Outputs)
  }
}
module.exports = ServerlessCfVars

